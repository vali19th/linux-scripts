#!/bin/bash
declare repositories=(
    danielrichter2007/grub-customizer
    flexiondotorg/youtube-dl-gui
    graphics-drivers/ppa
    linuxgndu/sqlitebrowser
    obsproject/obs-studio
    paolorotolo/android-studio
    webupd8team/atom
    webupd8team/java
)

declare packages=(
    # drivers
    build-essential
    intel-microcode
    nvidia-378
    nvidia-cuda-toolkit
    nvidia-modprobe
    nvidia-settings

    # system
    gparted
    grub-customizer
    mint-meta-codecs
    pavucontrol
    ttf-mscorefonts-installer

    # tools
    calligra
    etcher-electron
    nitroshare
    obs-studio
    remmina
    youtube-dlg

    # development
    android-studio
    apache2
    fasm
    libsdl2-dev
    mysql-server
    oracle-java8-installer
    oracle-java8-set-default
    qtcreator
    sqlite3
    sqlitebrowser
    valgrind
    vim

    # art
    atom
    audacity
    blender
    gimp
    inkscape
    krita
    qtractor

    # general
    anki
    chromium-browser
    deluge
    playonlinux
    skypeforlinux
    steam
)


# add repositories
for i in ${repositories[@]}; do
    sudo apt add-repository ppa:$i -y; done

# add etcher repository to sources
sudo su -c "echo 'deb https://dl.bintray.com/resin-io/debian stable etcher' >> /etc/apt/sources.list.d/etcher.list"
sudo apt-key adv --keyserver hkp://pgp.mit.edu:80 --recv-keys 379CE192D401AB61

# consider recommended packages as dependencies when installing apps
sudo sed -i 's/false/true/g' /etc/apt/apt.conf.d/00recommends

# auto accept licenses
echo oracle-java8-installer shared/accepted-oracle-license-v1-1 select true | sudo /usr/bin/debconf-set-selections
echo ttf-mscorefonts-installer msttcorefonts/present-mscorefonts-eula select true | sudo debconf-set-selections
echo ttf-mscorefonts-installer msttcorefonts/accepted-mscorefonts-eula select true | sudo debconf-set-selections

# install packages
sudo apt update
sudo apt upgrade
for i in ${packages[@]}; do
    sudo apt install -y $i; done



sudo apt autoremove -y
sudo apt clean
echo "$(tput setaf 6) Done! $(tput sgr0) [install.sh]"
/bin/bash
