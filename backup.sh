#!/bin/bash
declare atom_files=(
    config.cson
    init.coffee
    keymap.cson
    projects.cson
    snippets.cson
)

# backup atom files
rm -rf ./configs/atom
mkdir ./configs/atom
for i in ${atom_files[@]}; do
    cp /home/vali/.atom/$i 	./configs/atom/$i; done

# backup default apps
cp /etc/gnome/defaults.list	./configs/system/defaults.list

# backup startup apps
cp ~/.config/autostart/* ./configs/system/startup/

# backup steam controller config
cp /lib/udev/rules.d/99-steam-controller-perms.rules ./configs/steam_controller



echo "$(tput setaf 6) Done! $(tput sgr0) [backup.sh]"
/bin/bash
