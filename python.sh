#!/bin/bash
declare pip=(
    cython
    distribute
    pip
    setuptools
    virtualenv

    django
    flake8
    matplotlib
    nose
    pyqt5
    pysdl2
    scipy
)

declare apt=()
    python3-pip
)

# install packages
for i in ${apt[@]}; do
    sudo apt install -y $i; done

for i in ${pip[@]}; do
    pip3 install --user --upgrade $i; done



echo "$(tput setaf 6) Done! $(tput sgr0) [install.sh]"
/bin/bash
