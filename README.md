# Linux scripts

Tested on: Linux Mint 18.1 KDE (64 bits)  
I use those scripts to automate installations, configs and backups.  
You may need to modify some commands for them to work properly.  
Any tips and corrections are welcome.  

    TODO:  
        system settings
        apps settings
        panel layout (try: dconf dump /org/cinnamon/ > mysettings and dconf load /org/cinnamon/ < mysettings)
        restore.sh: take care of Qtractor, steam and mysql prompts
        configure all keybinds(switch windows/virtual desktop, start apps, toggle fullscreen, keep above/below, esc<->caps, etc)
            - OS specific keybinds -- use the super key for combinations

        vulkan - find a way to download latest version using console
        vulkan - verify the installation using VIA or the cube example https://vulkan.lunarg.com/doc/view/1.0.42.1/linux/getting_started.html#user-content-set-up-the-runtime-environment

        atom:
            - add keybinds for atom-terminal and git-plus
            - reconfigure the G1 -> G5 keybinds
