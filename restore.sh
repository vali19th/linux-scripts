#!/bin/bash
declare folders=(
    Documents
    Downloads
    Music
    Pictures
    Software
    Templates
    Videos
)

# automount DATA partition
DATA=sda9
sudo mkdir /mnt/$DATA
sudo su -c "echo '/dev/$DATA /mnt/$DATA auto nosuid,nodev,nofail,x-gvfs-show,uid=1000 0 0' >> /etc/fstab"
sudo mount /mnt/$DATA

# link folders between DATA and /home
cd ~/
rm -rf ~/Public
for i in ${folders[@]}; do
    rm -rf $i
    ln -sf $DATA/$i ~/$i
done

# install and configure software
sudo ./apt.sh
sudo ./atom.sh
sudo ./python.sh
sudo ./vulkan.sh

# configure terminal
sudo bash -c "echo 'PROMPT_DIRTRIM=1' >> ~/.bashrc"
echo '
alias open=xdg-open
alias py=python3
alias up=sudo apt update && sudo apt upgrade
' >> ~/.bash_aliases

# configure default apps
sudo cp ./configs/system/defaults.list	/etc/gnome/defaults.list

# configure startup apps
rm -rf ~/.config/autostart
mkdir ~/.config/autostart
cp  ./configs/system/startup/* ~/.config/autostart/

# configure steam controller
sudo cp ./configs/steam_controller /lib/udev/rules.d/99-steam-controller-perms.rules

# fix steam indicator not working in KDE 5
sudo apt install libdbusmenu-gtk4:i386 -y



sudo apt autoremove -y
sudo apt clean
echo "$(tput setaf 6) Done! $(tput sgr0) [install.sh]"
/bin/bash
