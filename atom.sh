#!/bin/bash
declare with_apm=(
    atom-beautify
    atom-terminal
    atom-trello
    autocomplete-java
    autocomplete-python
    busy-signal
    color-picker
    emmet
    file-icons
    git-plus
    highlight-line
    highlight-selected
    intentions
    linter
    linter-flake8
    linter-gcc
    linter-javac
    linter-ui-default
    monokai-arc
    project-manager
    sort-lines
    tree-view-autoresize
)

declare atom_files=(
    config.cson
    init.coffee
    keymap.cson
    projects.cson
    snippets.cson
)

# fix atom not starting properly
sudo rm -rf ./.atom

# fix atom not updating packages
sudo chown -R $USER ~/.atom

# install atom packages
for i in ${with_apm[@]}; do
    sudo apm install $i; done

# configure atom
for i in ${atom_files[@]}; do
    cp ./configs/atom/$i /home/vali/.atom/$i; done
