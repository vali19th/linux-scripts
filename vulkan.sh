#!/bin/bash
declare vk_dependencies=(
    cmake
    git
    graphviz
    libglm-dev
    libmirclient-dev
    libpciaccess0
    libpng-dev
    libpython2.7
    libwayland-dev
    libx11-dev
    libxcb-dri3-0
    libxcb-dri3-dev
    libxcb-present0
    libxrandr-devs
)

# install vulkan
for i in ${vk_dependencies[@]}; do
    sudo apt install -y $i; done

rm -rf VulkanSDK vulkan*
cp ./apps/vulkan*.run ./Software/
chmod ugo+x ~/Software/vulkan*.run
sudo ~/Software/vulkan*.run
rm -f ~/Software/vulkan*.run

source ~/Software/VulkanSDK/1.*/setup-env.sh



sudo apt autoremove -y
sudo apt clean
echo "$(tput setaf 6) Done! $(tput sgr0) [install.sh]"
/bin/bash
